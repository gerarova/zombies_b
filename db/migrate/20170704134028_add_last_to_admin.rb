class AddLastToAdmin < ActiveRecord::Migration[5.0]
  def change
    add_column :admins, :last, :string
  end
end
